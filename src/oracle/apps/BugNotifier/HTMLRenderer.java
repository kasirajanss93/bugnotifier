package oracle.apps.BugNotifier;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.PrintWriter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

public class HTMLRenderer {
	private static String HTML_FILE_NAME;
	private static String DESCRIPTION_FILE_NAME;
	private static String LATEST_DESCRIPTION_FILE_NAME;
	private static String PROGRAMMER_PROP_FILE_NAME;
	private static String MGR_PROP_FILE_NAME;
	private static String CC_PROP_FILE_NAME;

	public HTMLRenderer(String bugPropertiesFileName) {

		Properties properties = new Properties();
		FileInputStream inputProg;
		try {
			inputProg = new FileInputStream(bugPropertiesFileName);
			properties.load(inputProg);

			HTML_FILE_NAME = properties.getProperty("htmlFileName");
			DESCRIPTION_FILE_NAME = properties.getProperty("descriptionFileName");
			LATEST_DESCRIPTION_FILE_NAME = properties.getProperty("latestDescriptionFileName");
			PROGRAMMER_PROP_FILE_NAME = properties.getProperty("programmerPropFileName");
			MGR_PROP_FILE_NAME = properties.getProperty("mgrPropFileName");
			CC_PROP_FILE_NAME = properties.getProperty("ccPropFileName");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private String[] getSelectList(String selectClause) {

		String[] parts = selectClause.split(",");
		for (int i = 0; i < parts.length; i++) {
			parts[i] = parts[i].replaceAll("\\s+", "");
		}
		return parts;
	}

	String getTableHeader(Map columnMapping) {

		Iterator it = columnMapping.entrySet().iterator();
		String htmlHeader = "<html><head>";
		htmlHeader += "<title>Equivalent HTML</title>";
		htmlHeader += "</head><body>";
		htmlHeader += "<div style=\"overflow-x:auto;\"><table align=\"left\" border=\"1\" cellpadding=\"1\" cellspacing=\"1\" style=\"width: 100%;\">";
		String tableRow = "<tr>\n";
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			tableRow += "<td>\n" + "<p><strong>" + pair.getKey() + "</strong></p>\n" + "</td>\n";
			// System.out.println(pair.getKey() + " = " + pair.getValue());
			// it.remove(); // avoids a ConcurrentModificationException
		}
		tableRow += "</tr>\n";
		htmlHeader += tableRow;

		return htmlHeader;
	}

	String getTableRow(Map columnMapping) {

		Iterator it = columnMapping.entrySet().iterator();
		String tableRow = "<tr>\n";
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			if (pair.getKey().equals("RPTNO")) {
				tableRow += "<td valign=\"top\">\n" + "<p><strong>"
						+ "<a href=\"https://bug.oraclecorp.com/pls/bug/webbug_edit.edit_info_top?rptno="
						+ pair.getValue() + "\">" + pair.getValue() + "</a>" + "</strong></p>\n" + "</td>\n";
			} else {
				tableRow += "<td>\n" + "<p><strong>" + pair.getValue() + "</strong></p>\n" + "</td>\n";
			}
			// System.out.println(pair.getKey() + " = " + pair.getValue());
			// it.remove(); // avoids a ConcurrentModificationException
		}

		return tableRow;
	}

	void printMap(Map mp) {
		Iterator it = mp.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry pair = (Map.Entry) it.next();
			System.out.println(pair.getKey() + " = " + pair.getValue());
			// it.remove(); // avoids a ConcurrentModificationException
		}
	}

	void setBugDescriptionToFile(String bugList, String type, PrintWriter descFileWriter, Connection connection)
			throws Exception {
		String[] bugNum = null;
		bugNum = bugList.split(",");
		PreparedStatement stmtBugDesc = null;
		Map<String, StringBuilder> columnMapping = new LinkedHashMap<String, StringBuilder>();
		for (int i = 0; i < bugNum.length; i++) {

			System.out.println("Bug Num [i]: " + bugNum[i]);
			String rptno = bugNum[i];
			java.util.Date utilDate = new java.util.Date();
			final long MILLIS_IN_A_DAY = 1000 * 60 * 60 * 24 * 2;
			java.util.Date daysAgo = new Date(utilDate.getTime() - MILLIS_IN_A_DAY);
			java.sql.Date sqlDate = new java.sql.Date(daysAgo.getTime());
			System.out.println("utilDate:" + utilDate);
			System.out.println("sqlDate:" + sqlDate);
			String sqlBugDesc = "SELECT rptbody.rptno,\n" + "  rptbody.lineno,\n" + "  rptbody.comments\n"
					+ "FROM rptbody\n" + "WHERE rptbody.rptno = " + rptno + "\n" + "ORDER BY rptbody.rptno,\n"
					+ "  rptbody.lineno ";
			String sqlBugDescLatest = "SELECT *\n" + "FROM rptbody\n" + "WHERE rptbody.rptno = " + rptno + "\n"
					+ "AND upd_date        > TO_DATE('" + sqlDate + "','yyyy-mm-dd')";
			if (type.equals("description"))
				stmtBugDesc = connection.prepareStatement(sqlBugDesc);
			else if (type.equals("latestDescription"))
				stmtBugDesc = connection.prepareStatement(sqlBugDescLatest);
			ResultSet rsBugDesc = null;
			rsBugDesc = stmtBugDesc.executeQuery();
			String sqlBugSubject = " select subject from rpthead where rptno=" + rptno;
			PreparedStatement stmtBugSubject = connection.prepareStatement(sqlBugSubject);
			ResultSet rsBugSub = stmtBugSubject.executeQuery();
			String bugSubject = null;
			if (rsBugSub.next()) {
				bugSubject = rsBugSub.getString(1);
			}
			rsBugSub.close();
			StringBuilder sbDesc = new StringBuilder();
			sbDesc.append(bugSubject + "</br>");
			while (rsBugDesc.next()) {
				sbDesc.append(rsBugDesc.getString("COMMENTS"));
				sbDesc.append("</br>");
			}
			sbDesc.append(bugSubject + "</br>");
			StringBuilder sbBug = new StringBuilder();
			sbBug.append(rptno);
			columnMapping.put("RPTNO", sbBug);
			columnMapping.put("DESCRIPTION", sbDesc);
			if (i == 0)
				descFileWriter.print(getTableHeader(columnMapping));
			descFileWriter.print(getTableRow(columnMapping));
			rsBugDesc.close();

		}
		descFileWriter.print("</table></div></body></html>");
		descFileWriter.close();
	}

	void processXMLValues(Map bugInfoValues) throws Exception {

		String bugInfoId = (String) bugInfoValues.get("Id");
		String selectClause = (String) bugInfoValues.get("selectClause");
		String fromClause = (String) bugInfoValues.get("fromClause");
		String whereClause = (String) bugInfoValues.get("whereClause");
		String notificationInterval = (String) bugInfoValues.get("notificationInterval");
		String description = (String) bugInfoValues.get("description");
		String mailContent = (String) bugInfoValues.get("mailContent");
		String name = (String) bugInfoValues.get("Name");
		String toAddress = (String) bugInfoValues.get("toAddress");
		String ccAddress = (String) bugInfoValues.get("ccAddress");
		String bccList = (String) bugInfoValues.get("bccAddress");
		String latestDescription = (String) bugInfoValues.get("latestDescription");
		String mailSubject = (String) bugInfoValues.get("mailSubject");
		String overrideAddress = (String) bugInfoValues.get("overrideAddress");
		String sendMailIndividually = (String) bugInfoValues.get("sendMailIndividually");

		ConnectToDB connect = new ConnectToDB();
		String sql = connect.generateSQL(bugInfoId, selectClause, fromClause, whereClause, notificationInterval);
		Connection connection = ConnectToDB.getBugDBConnection();
		PreparedStatement stmt = null;
		stmt = connection.prepareStatement(sql);
		ResultSet rs = stmt.executeQuery();
		ResultSetMetaData meta = rs.getMetaData();
		Integer columncount = meta.getColumnCount();
		int count = 1;
		int headerCount = 1;
		File htmlFile = new File(HTML_FILE_NAME);
		htmlFile.getParentFile().mkdirs();
		PrintWriter htmlWriter = null;
		PrintWriter descFileWriter = null;
		PrintWriter latestDescFileWriter = null;
		Properties propertiesProg = new Properties();
		Properties propertiesMgr = new Properties();
		Properties propertiesCCList = new Properties();
		if (!PROGRAMMER_PROP_FILE_NAME.equals("null")) {
			FileInputStream inputProg = new FileInputStream(PROGRAMMER_PROP_FILE_NAME);
			propertiesProg.load(inputProg);
		}
		if (!MGR_PROP_FILE_NAME.equals("null")) {
			FileInputStream inputMgr = new FileInputStream(MGR_PROP_FILE_NAME);
			propertiesMgr.load(inputMgr);
		}
		if (!CC_PROP_FILE_NAME.equals("null")) {
			FileInputStream inputCC = new FileInputStream(CC_PROP_FILE_NAME);
			propertiesCCList.load(inputCC);
		}
		String programmerEmail = null;
		System.out.println("programmerEmail :" + programmerEmail);
		String assignList = null, mgrList = null;
		String mgrEmail = null;
		Boolean isFirst = false, isMgrFirst = false;
		String ccList = null;
		String escalationContact = null;
		if (!CC_PROP_FILE_NAME.equals("null"))
			escalationContact = propertiesCCList.getProperty("escalationContact");
		String bugList = null;

		if (sendMailIndividually != null && sendMailIndividually.equals("yes")) {
			while (rs.next()) {
				String bugNO = rs.getString("RPTNO");
				count = 1;
				Map columnMapping = new LinkedHashMap();
				for (int j = 0; j < columncount; j++) {
					columnMapping.put(meta.getColumnLabel(count), rs.getString(meta.getColumnLabel(count)));
					System.out.println(meta.getColumnLabel(count) + ":" + rs.getString(meta.getColumnLabel(count)));
					count++;
				}
				htmlWriter = new PrintWriter(HTML_FILE_NAME, "UTF-8");
				descFileWriter = new PrintWriter(DESCRIPTION_FILE_NAME, "UTF-8");
				latestDescFileWriter = new PrintWriter(LATEST_DESCRIPTION_FILE_NAME, "UTF-8");
				htmlWriter.print(getTableHeader(columnMapping));
				htmlWriter.print(getTableRow(columnMapping));
				htmlWriter.print("</table></div></body></html>");
				htmlWriter.close();
				System.out.println("------------------------------------");
				printMap(columnMapping);
				System.out.println("------------------------------------");
				if (!PROGRAMMER_PROP_FILE_NAME.equals("null"))
					programmerEmail = propertiesProg.getProperty(columnMapping.get("PROGRAMMER").toString());
				if (!MGR_PROP_FILE_NAME.equals("null"))
					mgrEmail = propertiesMgr.getProperty(columnMapping.get("PROGRAMMER").toString());
				if (programmerEmail != null) {

					assignList = programmerEmail;
					if (isMgrFirst == false && mgrEmail != null) {
						mgrList = mgrEmail;
						isMgrFirst = true;
					}
					if (description != null && description.equals("yes") && bugNO != null) {
						System.out.println("Description------------");
						setBugDescriptionToFile(bugNO, "description", descFileWriter, connection);
					}

					if (latestDescription != null && latestDescription.equals("yes") && bugNO != null) {
						System.out.println("Latest Description------------");
						setBugDescriptionToFile(bugNO, "latestDescription", latestDescFileWriter, connection);
					}

					if (isMgrFirst == false)
						ccList = escalationContact;
					else
						ccList = escalationContact + "," + mgrList;
					System.out.println("IC Assign: " + assignList + "CC List: " + ccList);
					Email mail = new Email();
					if (overrideAddress != null && overrideAddress.equals("yes")) {
						if (toAddress != null)
							assignList = toAddress;
						if (ccAddress != null)
							ccList = ccAddress;
					} else {
						assignList += "," + toAddress;
						ccList += "," + ccAddress;
					}
					System.out.println("Final IC Assign: " + assignList + "Final CC List: " + ccList);
					System.out.println("CCAddress: " + ccAddress);
					if (bugNO != null) {
						String finalSubject = null;
						if (mailSubject != null) {
							finalSubject = mailSubject.replace("?", bugNO);
						}
						System.out.println("Mail Subject:" + mailSubject);
						mail.sendEmail(assignList, ccList, bccList, HTML_FILE_NAME, mailContent, finalSubject,
								description, DESCRIPTION_FILE_NAME, latestDescription, LATEST_DESCRIPTION_FILE_NAME);

					}

				} else
					continue;

			}

			rs.close();
			stmt.close();

			connection.close();

		}

		else {
			htmlWriter = new PrintWriter(HTML_FILE_NAME, "UTF-8");
			descFileWriter = new PrintWriter(DESCRIPTION_FILE_NAME, "UTF-8");
			latestDescFileWriter = new PrintWriter(LATEST_DESCRIPTION_FILE_NAME, "UTF-8");
			while (rs.next()) {
				count = 1;
				Map columnMapping = new LinkedHashMap();
				for (int j = 0; j < columncount; j++) {
					columnMapping.put(meta.getColumnLabel(count), rs.getString(meta.getColumnLabel(count)));
					System.out.println(meta.getColumnLabel(count) + ":" + rs.getString(meta.getColumnLabel(count)));
					count++;
				}
				if (headerCount == 1) {
					bugList = rs.getString("RPTNO");
					htmlWriter.print(getTableHeader(columnMapping));
					htmlWriter.print(getTableRow(columnMapping));
					headerCount++;
				} else {
					bugList += "," + rs.getString("RPTNO");
					htmlWriter.print(getTableRow(columnMapping));
				}
				System.out.println("------------------------------------");
				printMap(columnMapping);
				System.out.println("------------------------------------");

				if (!PROGRAMMER_PROP_FILE_NAME.equals("null"))
					programmerEmail = propertiesProg.getProperty(columnMapping.get("PROGRAMMER").toString());
				if (!MGR_PROP_FILE_NAME.equals("null"))
					mgrEmail = propertiesMgr.getProperty(columnMapping.get("PROGRAMMER").toString());

				if (programmerEmail != null) {
					if (isFirst == false) {
						assignList = programmerEmail;
						isFirst = true;
						if (isMgrFirst == false && mgrEmail != null) {
							mgrList = mgrEmail;
							isMgrFirst = true;
						}
					} else {
						if (!assignList.toLowerCase().contains(programmerEmail.toLowerCase()))
							assignList += "," + programmerEmail;
						if (isMgrFirst == false && mgrEmail != null) {
							mgrList = mgrEmail;
							isMgrFirst = true;
						} else {
							if (mgrEmail != null && !mgrList.toLowerCase().contains(mgrEmail.toLowerCase())) {
								mgrList += "," + mgrEmail;
							}
						}
					}
				} else
					continue;

			}
			htmlWriter.print("</table></div></body></html>");
			rs.close();
			stmt.close();
			htmlWriter.close();
			if (description != null && description.equals("yes") && bugList != null) {
				System.out.println("Description------------");
				setBugDescriptionToFile(bugList, "description", descFileWriter, connection);
			}

			if (latestDescription != null && latestDescription.equals("yes") && bugList != null) {
				System.out.println("Latest Description------------");
				setBugDescriptionToFile(bugList, "latestDescription", latestDescFileWriter, connection);
			}

			connection.close();

			if (isMgrFirst == false)
				ccList = escalationContact;
			else
				ccList = escalationContact + "," + mgrList;
			System.out.println("IC Assign: " + assignList + "CC List: " + ccList);
			Email mail = new Email();
			if (overrideAddress != null && overrideAddress.equals("yes")) {
				if (toAddress != null)
					assignList = toAddress;
				if (ccAddress != null)
					ccList = ccAddress;
			} else {
				assignList += "," + toAddress;
				ccList += "," + ccAddress;
			}
			System.out.println("Final IC Assign: " + assignList + "Final CC List: " + ccList);
			System.out.println("CCAddress: " + ccAddress);
			if (bugList != null) {
				if (mailSubject != null) {
					mailSubject = mailSubject.replace("?", bugList);
				}
				System.out.println("Mail Subject:" + mailSubject);
				mail.sendEmail(assignList, ccList, bccList, HTML_FILE_NAME, mailContent, mailSubject, description,
						DESCRIPTION_FILE_NAME, latestDescription, LATEST_DESCRIPTION_FILE_NAME);
			}

		}

	}

}