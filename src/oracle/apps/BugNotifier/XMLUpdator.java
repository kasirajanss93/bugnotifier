package oracle.apps.BugNotifier;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;

import org.w3c.dom.NodeList;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.Enumeration;


public class XMLUpdator extends HttpServlet {


    private static  String BUG_INFO_XML_FILE_NAME;
    private static  String BUG_PROPERTY =
        "/home/ksselvak/apache-tomcat-7.0.61/webapps/bugNotifierContents/properties/bugProperties.properties";


    public XMLUpdator() {
        Properties properties = new Properties();
        FileInputStream inputProg;
        try {
            inputProg = new FileInputStream(BUG_PROPERTY);
            properties.load(inputProg);


            BUG_INFO_XML_FILE_NAME = properties.getProperty("bugInfoFileName");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private  static String getElementValue(Element eElement, String eName) {
        if (eElement.getElementsByTagName(eName) != null &&
            eElement.getElementsByTagName(eName).item(0) != null)
            return eElement.getElementsByTagName(eName).item(0).getTextContent();
        return null;
    }


    public  void printMap(Map mp) {
        Iterator it = mp.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            System.out.println(pair.getKey() + " = " + pair.getValue());
            // it.remove(); // avoids a ConcurrentModificationException
        }
    }


    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                               IOException {
        // reading the user input
        PrintWriter out = response.getWriter();
        response.setContentType("text/plain");

        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String> bugInfoValues =
            new LinkedHashMap<String, String>();
        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();
            out.write(paramName);
            out.write(": ");

            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                out.write(paramValue);
                out.write("\n");
                bugInfoValues.put(paramName, paramValue);
            }
        }

        try {
            updateBugXML(bugInfoValues);
            out.write("Updated Successfully");
        } catch (Exception e) {
        }
    }


    public static void updateBugXML(Map bugInformation) throws Exception {

        File inputFile = new File(BUG_INFO_XML_FILE_NAME);
        DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        System.out.println("Root element :" +
                           doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName("BugInformation");
        System.out.println("----------------------------");
        //loop through the xml and find the BugInformation Tag
        Map<String, String> bugInfoValues =
            new LinkedHashMap<String, String>();
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            System.out.println("\nCurrent Element :" + nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element)nNode;
                String bugInfoId = eElement.getAttribute("Id");
                // Get the value of each node
                if (bugInfoId.equals((String)bugInformation.get("Id"))) {
                    NodeList bugInfoChild = nNode.getChildNodes();
                    for (int j = 0; j < bugInfoChild.getLength(); j++) {
                        Node bugInfoChildNode = bugInfoChild.item(j);
                        String finalValue =
                            (String)bugInformation.get((String)bugInfoChildNode.getNodeName());
                        bugInfoChildNode.setTextContent(finalValue);


                    }
                }

            }

        }


        TransformerFactory transformerFactory =
            TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                                      "2");
        DOMSource source = new DOMSource(doc);
        StreamResult result =
            new StreamResult(new File(BUG_INFO_XML_FILE_NAME));
        transformer.transform(source, result);
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(source, consoleResult);


    }


    public  static Map<String, String> getBugInformation(String id) throws Exception {
        File inputFile = new File(BUG_INFO_XML_FILE_NAME);
        DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        System.out.println("Root element :" +
                           doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName("BugInformation");
        System.out.println("----------------------------");
        //loop through the xml and find the BugInformation Tag
        Map<String, String> bugInfoValues =
            new LinkedHashMap<String, String>();
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            System.out.println("\nCurrent Element :" + nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element)nNode;
                String bugInfoId = eElement.getAttribute("Id");
                // Get the value of each node
                if (bugInfoId.equals(id)) {
                    NodeList bugInfoChild = nNode.getChildNodes();
                    for (int j = 0; j < bugInfoChild.getLength(); j++) {
                        Node bugInfoChildNode = bugInfoChild.item(j);
                        bugInfoValues.put(bugInfoChildNode.getNodeName(),
                                          bugInfoChildNode.getTextContent());

                    }
                }

            }

        }
        return bugInfoValues;

    }


    public  static Map<String, String> getBugList() throws Exception {

        File inputFile = new File(BUG_INFO_XML_FILE_NAME);
        DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(inputFile);
        doc.getDocumentElement().normalize();
        System.out.println("Root element :" +
                           doc.getDocumentElement().getNodeName());
        NodeList nList = doc.getElementsByTagName("BugInformation");
        System.out.println("----------------------------");
        //loop through the xml and find the BugInformation Tag
        Map<String, String> bugInfoValues =
            new LinkedHashMap<String, String>();
        for (int i = 0; i < nList.getLength(); i++) {
            Node nNode = nList.item(i);
            System.out.println("\nCurrent Element :" + nNode.getNodeName());
            if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                Element eElement = (Element)nNode;
                String bugInfoId = eElement.getAttribute("Id");
                // Get the value of each node
                String name = getElementValue(eElement, "Name");
                bugInfoValues.put(bugInfoId, name);

            }

        }
        return bugInfoValues;

    }


 


}
