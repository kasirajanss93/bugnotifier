package oracle.apps.BugNotifier;

import java.io.File;

import java.io.FileInputStream;

import java.io.FileNotFoundException;

import java.text.DateFormat;

import java.text.SimpleDateFormat;

import java.util.Date;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import java.util.Properties;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.dom.DOMSource;

import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLProcessor {
    private static String BUG_INFO_XML_FILE_NAME;
    private static String HTML_FILE_NAME;
    private static String ERROR_INFO_XML_FILE_NAME;
    private static String DESCRIPTION_FILE_NAME;
    private static String BUG_PROPERTY;
    public XMLProcessor(String bugPropertiesFileName) {
      Properties properties = new Properties();
          FileInputStream inputProg;
        try {
            inputProg = new FileInputStream(bugPropertiesFileName);
            properties.load(inputProg);
          
          BUG_INFO_XML_FILE_NAME =properties.getProperty("bugInfoFileName");
          HTML_FILE_NAME = properties.getProperty("htmlFileName");
          ERROR_INFO_XML_FILE_NAME=properties.getProperty("errorInfoFileName");
          DESCRIPTION_FILE_NAME=properties.getProperty("descriptionFileName");
          BUG_PROPERTY=bugPropertiesFileName;
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        
       
    }


    private String getElementValue(Element eElement, String eName) {
        if( eElement.getElementsByTagName(eName)!=null&&eElement.getElementsByTagName(eName).item(0)!=null)
          return eElement.getElementsByTagName(eName).item(0).getTextContent();
        return null;
    }
    
    
    public void updateErrorXML(String id,String updateWhen) {
        try {
          File inputFile = new File(ERROR_INFO_XML_FILE_NAME);
          DocumentBuilderFactory dbFactory =
              DocumentBuilderFactory.newInstance();
          DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
          Document doc = dBuilder.parse(inputFile);
          doc.getDocumentElement().normalize();
          System.out.println("Root element :" +
                             doc.getDocumentElement().getNodeName());
          Node errorInformation = doc.getElementsByTagName("ErrorInformation").item(Integer.parseInt(id)-1);
          if (errorInformation.getNodeType() == Node.ELEMENT_NODE) {
              Element eElement = (Element)errorInformation;
              String bugInfoId = eElement.getAttribute("Id");
              System.out.println("Error Id : " + bugInfoId);
          }
          System.out.println("----------------------------");
          NodeList list = errorInformation.getChildNodes();
          DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
          //get current date time with Date()
          Date date = new Date();
          String str = dateFormat.format(date);
          Date successfulRunTime = null;
          for (int i = 0; i < list.getLength(); i++) {
            Node node = list.item(i);
              if(updateWhen.equals("RunTime")) {
                final long ONE_MIN_IN_MILLIS=60000;
                long currTimeInMs = date.getTime();
                Date subOnemin = new Date(currTimeInMs-ONE_MIN_IN_MILLIS);
                String dateStr = dateFormat.format(subOnemin);
                if ("lastRunTime".equals(node.getNodeName())) {
                    node.setTextContent(dateStr);
                }
              }
              else if(updateWhen.equals("Success")) {
                
                if ("lastSuccessfulRunTime".equals(node.getNodeName())) {
                    node.setTextContent(str);
                     }
              }
              else if(updateWhen.equals("Failure")) {
                if ("lastSuccessfulRunTime".equals(node.getNodeName())) {
                    String successTimeStr = node.getTextContent();
                    if(!successTimeStr.equals(""))
                    successfulRunTime = dateFormat.parse(successTimeStr);
                  
                }
                if ("lastFailureRunTime".equals(node.getNodeName())) {
                  String failureTimeStr = node.getTextContent();
                  Date failureTime=null;
                  if(!node.getTextContent().equals(""))
                    failureTime = dateFormat.parse(failureTimeStr);
                  if(node.getTextContent().equals("")|| successfulRunTime!=null&&failureTime.compareTo(successfulRunTime)<=0) 
                      node.setTextContent(str);
                }
            }
              
              
          }
            
          TransformerFactory transformerFactory = TransformerFactory.newInstance();
          Transformer transformer = transformerFactory.newTransformer();
          DOMSource source = new DOMSource(doc);
          StreamResult result = new StreamResult(new File(ERROR_INFO_XML_FILE_NAME));
          transformer.transform(source, result);  
            
            
        }
        catch (Exception e) {
          e.printStackTrace();
        }
       
       
       
    }
    
    
  void printMap(Map mp) {
          Iterator it = mp.entrySet().iterator();
          while (it.hasNext()) {
              Map.Entry pair = (Map.Entry)it.next();
              System.out.println(pair.getKey() + " = " + pair.getValue());
              // it.remove(); // avoids a ConcurrentModificationException
          }
      }
    
    
  public String getNotificationInterval(String bugInfoId,String notificationInterval,String startTimeStr) {

    Long finalNotificationInterval=null;
      try {
          File inputFile = new File(ERROR_INFO_XML_FILE_NAME);
          DocumentBuilderFactory dbFactory =
              DocumentBuilderFactory.newInstance();
          DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
          Document doc = dBuilder.parse(inputFile);
          doc.getDocumentElement().normalize();
          System.out.println("Root element :" +
                             doc.getDocumentElement().getNodeName());

          Node errorInformation =
              doc.getElementsByTagName("ErrorInformation").item(Integer.parseInt(bugInfoId) -
                                                                1);
          NodeList list = errorInformation.getChildNodes();
          String lastRunTimeStr = null, lastSuccessfulRunTimeStr =
              null, lastFailureRunTimeStr = null;
          for (int i = 0; i < list.getLength(); i++) {
              Node node = list.item(i);

              if ("lastRunTime".equals(node.getNodeName())) {
                  lastRunTimeStr = node.getTextContent();
              }
              if ("lastSuccessfulRunTime".equals(node.getNodeName())) {
                  lastSuccessfulRunTimeStr = node.getTextContent();
              }

              if ("lastFailureRunTime".equals(node.getNodeName())) {
                  lastFailureRunTimeStr = node.getTextContent();
              }
          }
          
          DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
          Date date = new Date();
          String str = dateFormat.format(date);
          Date timeNow = dateFormat.parse(str);
          Date lastRunTime=null,lastSuccessfulRunTime=null,lastFailureRunTime=null;
          // If the Query has never run, then append the value of the notification interval
          if(lastRunTimeStr.equals("")) {
        	 if(startTimeStr!=null&&!startTimeStr.equals("")) {
        		 Date startTime=dateFormat.parse(startTimeStr);
        		 if(timeNow.compareTo(startTime)>=0) finalNotificationInterval=Long.valueOf(notificationInterval);
        		 else finalNotificationInterval=Long.valueOf(0);
        	 }
        	 else  
        		 finalNotificationInterval=Long.valueOf(notificationInterval);
          }
          else {
            // If the query didn't fail at all then, TimeNow - LastRunTime should be more than the Given Notification Interval
            if(lastFailureRunTimeStr.equals("")) {
              lastRunTime=dateFormat.parse(lastRunTimeStr);
              long diff = timeNow.getTime() - lastRunTime.getTime();
              System.out.println("Diff MilliSeconds "+diff);
              long diffMinutes =  ((diff / 1000) / 60);//diff / (60 * 1000) % 60;
              System.out.println("diffMinutes: "+diffMinutes);
              if(diffMinutes>=Long.parseLong(notificationInterval)) {
                 finalNotificationInterval = diffMinutes; 
              }
              else finalNotificationInterval=Long.valueOf(0);
            }
            else {
              lastRunTime=dateFormat.parse(lastRunTimeStr);
              if(!lastSuccessfulRunTimeStr.equals(""))
                lastSuccessfulRunTime=dateFormat.parse(lastSuccessfulRunTimeStr);
              lastFailureRunTime=dateFormat.parse(lastFailureRunTimeStr);
                //If the Query has failed and the SuccessfulRunTime is less than the Failure Time then TimeNow - LastSuccessRunTime Should be the Notification Interval
                if(!lastSuccessfulRunTimeStr.equals("")&&lastSuccessfulRunTime.compareTo(lastFailureRunTime)<=0) {
                  long diff = timeNow.getTime() - lastSuccessfulRunTime.getTime();
                  System.out.println("Diff MilliSeconds: "+diff);
                  long diffMinutes =  ((diff / 1000) / 60);
                  System.out.println("diffMinutes: "+diffMinutes);
                  finalNotificationInterval=diffMinutes;
                }
                // If the Query has never ran Successfully then the Notification interval should be TimeNow - Last Failure Run Time
                else if(lastSuccessfulRunTimeStr.equals("")) {
                  long diff = timeNow.getTime() - lastFailureRunTime.getTime();
                  System.out.println("Diff MilliSeconds "+diff);
                  long diffMinutes =  ((diff / 1000) / 60);
                  System.out.println("diffMinutes: "+diffMinutes);
                  finalNotificationInterval=diffMinutes;
                }
                // Else if it has run successfully after failure then TimeNow - LastRunTime is the Notification Interval
                else {
                  long diff = timeNow.getTime() - lastRunTime.getTime();
                  System.out.println("Diff MilliSeconds: "+diff);
                  long diffMinutes =  ((diff / 1000) / 60);
                  System.out.println("diffMinutes: "+diffMinutes);
                  if(diffMinutes>=Long.parseLong(notificationInterval)) {
                     finalNotificationInterval = diffMinutes; 
                  }
                  else finalNotificationInterval=Long.valueOf(0);
                }   
            }
          }
         
          
          
       

          
      } catch (Exception e) {
          e.printStackTrace();
    
      }
      
     return finalNotificationInterval.toString();

  }
    
    
    

    public void processXML() {
        try {
            File inputFile = new File(BUG_INFO_XML_FILE_NAME);
            DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputFile);
            doc.getDocumentElement().normalize();
            System.out.println("Root element :" +
                               doc.getDocumentElement().getNodeName());
            NodeList nList = doc.getElementsByTagName("BugInformation");
            System.out.println("----------------------------");
            //loop through the xml and find the BugInformation Tag
            for (int i = 0; i < nList.getLength(); i++) {
                Node nNode = nList.item(i);
                System.out.println("\nCurrent Element :" +
                                   nNode.getNodeName());
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element)nNode;
                    String bugInfoId = eElement.getAttribute("Id");
                    // Get the value of each node
                    Map<String,String> bugInfoValues = new LinkedHashMap<String,String>();
                    bugInfoValues.put("Id",bugInfoId);
                    NodeList bugInfoChild = nNode.getChildNodes();
                    
                    for (int j = 0; j < bugInfoChild.getLength(); j++) {
                        Node bugInfoChildNode = bugInfoChild.item(j);
                        bugInfoValues.put(bugInfoChildNode.getNodeName(), bugInfoChildNode.getTextContent());
                        
                    }
                    
                    
                    printMap(bugInfoValues);
                    
                    

                    try {
                        HTMLRenderer htmlRenderer =
                            new HTMLRenderer(BUG_PROPERTY);
                        String newNotificationInterval=getNotificationInterval(bugInfoId,bugInfoValues.get("notificationInterval"),bugInfoValues.get("startTime"));
                        System.out.println("New Notification Interval: "+newNotificationInterval);
                        if(!newNotificationInterval.equals("0")) {
                          updateErrorXML(bugInfoId,"RunTime");
                          htmlRenderer.processXMLValues(bugInfoValues);
                          
                          updateErrorXML(bugInfoId,"Success");
                        }
                        

                    } catch (Exception e) {
                        e.printStackTrace();
                        updateErrorXML(bugInfoId,"Failure");
                    }

               }
             
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
