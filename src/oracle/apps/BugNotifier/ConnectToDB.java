package oracle.apps.BugNotifier;

import java.io.File;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

import java.util.Date;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ConnectToDB {


    public String replaceLast(String string, String toReplace,
                              String replacement) {
        int pos = string.lastIndexOf(toReplace);
        if (pos > -1) {
            return string.substring(0, pos) + replacement +
                string.substring(pos + toReplace.length(), string.length());
        } else {
            return string;
        }
    }
    

    public String generateSQL(String bugInfoId, String selectClause,
                              String fromClause, String whereClause,
                              String notificationInterval) {

      
        whereClause = replaceLast(whereClause, "?", notificationInterval);
        String sql =
            "select " + selectClause + " from " + fromClause + " where " +
            whereClause;

        System.out.println("Generated Sql :" + sql);

        return sql;
    }

    public static Connection getBugDBConnection() {
        try {

            Class.forName("oracle.jdbc.driver.OracleDriver");

        } catch (ClassNotFoundException e) {

            System.out.println("Where is your Oracle JDBC Driver?");
            e.printStackTrace();
            return null;

        }

        System.out.println("Oracle JDBC Driver Registered!");
        Connection connection = null;

        try {


        	 connection =
                     DriverManager.getConnection("",
                                                 "", "");


        } catch (SQLException e) {

            System.out.println("Connection Failed! Check output console");
            e.printStackTrace();
            return null;

        }

        if (connection != null) {
            return connection;
        } else {
            System.out.println("Failed to make connection!");
        }

        return connection;
    }


}


