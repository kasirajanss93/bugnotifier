package oracle.apps.BugNotifier;

import java.io.PrintStream;

import javax.mail.Session;

import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

import javax.activation.*;


public class Email {
    
    private static final String FROM_ADDRESS="Kasi<ksselvak@slc06xgl.us.oracle.com>";


    private void addAttachment(Multipart multipart,String fileName) throws Exception {
        DataSource source = new FileDataSource(fileName);
        BodyPart messageBodyPart = new MimeBodyPart();
        messageBodyPart.setDataHandler(new DataHandler(source));
        messageBodyPart.setFileName(fileName);
        multipart.addBodyPart(messageBodyPart);

    }
    
    public void sendEmail(String toAddress, String ccAddress,String bccAddress, String attachmentFilePath,String mailContent,String mailSubject,String description,String descFileName,String latestDescription,String latestDescFileName) throws Exception{
        // Recipient's email ID needs to be mentioned.
        String to = toAddress;

        String cc = ccAddress;
        // Sender's email ID needs to be mentioned
        String from = FROM_ADDRESS;

        // Assuming you are sending email from localhost
        String host = "localhost";

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", host);

        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

     
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipients(Message.RecipientType.TO,
                                 InternetAddress.parse(to));
            message.addRecipients(Message.RecipientType.CC,
                                 InternetAddress.parse(cc));
            
            if(bccAddress!=null&&!bccAddress.equals(""))
            message.addRecipients(Message.RecipientType.BCC,
                           InternetAddress.parse(bccAddress));

            // Set Subject: header field
            message.setSubject(mailSubject);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText(mailContent);

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            addAttachment(multipart,attachmentFilePath);
            
            
            if(description!=null&&description.equals("yes")) {
                addAttachment(multipart,descFileName);
            }
            
            if(latestDescription!=null&&latestDescription.equals("yes")) {
              addAttachment(multipart,latestDescFileName);
            }
            
            // Send the complete message parts
            message.setContent(multipart);

            System.out.println("\n"+message);
            // Send message
            Transport.send(message);
            System.out.println("Sent message successfully....");
       
    }
    
    
}
