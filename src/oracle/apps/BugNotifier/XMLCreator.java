package oracle.apps.BugNotifier;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.PrintWriter;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.OutputKeys;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;

import java.util.Enumeration;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

import java.util.Properties;

import javax.servlet.ServletConfig;

import org.w3c.dom.NodeList;

public class XMLCreator extends HttpServlet {

    private static  String BUG_INFO_XML_FILE_NAME;
    private static  String ERROR_INFO_XML_FILE_NAME;
    private static  String BUG_PROPERTY =
        "/home/ksselvak/apache-tomcat-7.0.61/webapps/bugNotifierContents/properties/bugProperties.properties";

    public XMLCreator() {
        Properties properties = new Properties();
        FileInputStream inputProg;
        try {
        	
            inputProg = new FileInputStream(BUG_PROPERTY);
            properties.load(inputProg);


            BUG_INFO_XML_FILE_NAME = properties.getProperty("bugInfoFileName");
            ERROR_INFO_XML_FILE_NAME =
                    properties.getProperty("errorInfoFileName");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected void doPost(HttpServletRequest request,
                          HttpServletResponse response) throws ServletException,
                                                               IOException {
        // reading the user input
        PrintWriter out = response.getWriter();
        response.setContentType("text/plain");

        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String> bugInfoValues =
            new LinkedHashMap<String, String>();
        while (parameterNames.hasMoreElements()) {

            String paramName = parameterNames.nextElement();
            out.write(paramName);
            out.write(": ");

            String[] paramValues = request.getParameterValues(paramName);
            for (int i = 0; i < paramValues.length; i++) {
                String paramValue = paramValues[i];
                out.write(paramValue);
                out.write("\n");
                bugInfoValues.put(paramName, paramValue);
            }
        }


        try {
            buildBugInfoXml(bugInfoValues);
            out.println("Successful");
        } catch (Exception e) {
            out.print(e);
        }

    }


    private void createBugInfoElement(Document doc, Element bugInformation,
                                      String xmlNodeName,
                                      String xmlNodeValue) {
        Element xmlNode = doc.createElement(xmlNodeName);
        if (xmlNodeValue != null)
            xmlNode.appendChild(doc.createTextNode(xmlNodeValue));
        bugInformation.appendChild(xmlNode);
        //Attr attrType = doc.createAttribute("type");
        //attrType.setValue("formula one");
        //name.setAttributeNode(attrType);


    }


    void buildBugInfoXml(Map bugInfoValues) throws Exception {


        DocumentBuilderFactory dbFactory =
            DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = null;
        File f = new File(BUG_INFO_XML_FILE_NAME);
        if (!f.exists() && !f.isDirectory()) {
            f.getParentFile().mkdirs();
            FileWriter writer = new FileWriter(f);
        }

        FileReader fr = new FileReader(f);
        if (fr.read() == -1) {
            doc = dBuilder.newDocument();
            Element rootElement = doc.createElement("BugNotifier");
            doc.appendChild(rootElement);
        } else {
            doc = dBuilder.parse(f);
            // root element
            if (doc.getDocumentElement() == null) {
                Element rootElement = doc.createElement("BugNotifier");
                doc.appendChild(rootElement);
            }
        }

        //  buInformation element
        Element bugInformation = doc.createElement("BugInformation");
        doc.getDocumentElement().appendChild(bugInformation);
        NodeList list = doc.getElementsByTagName("BugInformation");

        // setting attribute to element
        Attr attr = doc.createAttribute("Id");
        String id = "" + list.getLength();
        attr.setValue(id);
        bugInformation.setAttributeNode(attr);

        Iterator it = bugInfoValues.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if (!pair.getKey().equals("#text")) {
                createBugInfoElement(doc, bugInformation,
                                     (String)pair.getKey(),
                                     (String)pair.getValue());
            }
        }

        // elements of buginformation

        // write the content into xml file
        TransformerFactory transformerFactory =
            TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                                      "2");

        DOMSource source = new DOMSource(doc);
        StreamResult result =
            new StreamResult(new File(BUG_INFO_XML_FILE_NAME));
        transformer.transform(source, result);
        // Output to console for testing
        StreamResult consoleResult = new StreamResult(System.out);
        transformer.transform(source, consoleResult);
        buildErrorInfoXml(id);


    }


    void buildErrorInfoXml(String id) {
        try {
            DocumentBuilderFactory dbFactory =
                DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = null;
            File f = new File(ERROR_INFO_XML_FILE_NAME);
            if (!f.exists() && !f.isDirectory()) {
                f.getParentFile().mkdirs();
                FileWriter writer = new FileWriter(f);
            }

            FileReader fr = new FileReader(f);
            if (fr.read() == -1) {
                doc = dBuilder.newDocument();
                Element rootElement = doc.createElement("BugNotifierError");
                doc.appendChild(rootElement);
            } else {
                doc = dBuilder.parse(f);
                // root element
                if (doc.getDocumentElement() == null) {
                    Element rootElement =
                        doc.createElement("BugNotifierError");
                    doc.appendChild(rootElement);
                }
            }

            Element errorInformation = doc.createElement("ErrorInformation");
            doc.getDocumentElement().appendChild(errorInformation);
            Attr attr = doc.createAttribute("Id");
            attr.setValue(id);
            errorInformation.setAttributeNode(attr);

            createBugInfoElement(doc, errorInformation, "lastRunTime", null);
            createBugInfoElement(doc, errorInformation,
                                 "lastSuccessfulRunTime", null);
            createBugInfoElement(doc, errorInformation, "lastFailureRunTime",
                                 null);

            TransformerFactory transformerFactory =
                TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount",
                                          "2");

            DOMSource source = new DOMSource(doc);
            StreamResult result =
                new StreamResult(new File(ERROR_INFO_XML_FILE_NAME));
            transformer.transform(source, result);
            // Output to console for testing
            StreamResult consoleResult = new StreamResult(System.out);
            transformer.transform(source, consoleResult);


        }

        catch (Exception e) {
            e.printStackTrace();
        }

    }


}
