<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title> Bug Notifier   </title>
		 <link rel="stylesheet" type="text/css" href="style.css" media="all" />
    	<link rel="stylesheet" type="text/css" href="demo.css" media="all" />
	</head>	
	<body>
		<div class="container">		
			<header>
				<a href="/bugNotifier"><h1>Bug Notifier</h1></a>
            </header>
            <div  class="form">
            	<form id="contactform" action="XMLCreator"  method="POST">
            	<!--	 <p class="contact"><label for="name">Bug Notifier Name </label></p>
            		 <textarea rows="4" cols="50" required="" name="comment"></textarea></br> !-->
            		 <p class="contact"><label for="name">Bug Notifier Name </label></p>
					 <input type="text" required="" name="Name"size="20px">
					  <p class="contact"><label for="name">Select Clause  </label></p>
					  <textarea rows="4" cols="50" required="" name="selectClause"></textarea>
					 <p class="contact"><label for="name">From Clause</label></p> 
					 <textarea rows="4" cols="50" required="" name="fromClause"></textarea>
					 <p class="contact"><label for="name">Where Clause</label></p> 
					 <textarea rows="4" cols="50" required="" name="whereClause"></textarea>
					<p class="contact"><label for="name">Description</label></p>
					<select required="" name="description" >
						<option value="no">no</option>
						<option value="yes">yes</option>
					</select>
					<p class="contact"><label for="name">Latest Description</label></p> 
					<select required="" name="latestDescription" >
						<option value="no">no</option>
						<option value="yes">yes</option>
					</select>
					<p class="contact"><label for="name">Mail Subject  </label></p>
					<input type="text" required="" name="mailSubject"size="20px">
					 <p class="contact"><label for="name">Mail Content  </label></p>
					<input type="text" required="" name="mailContent"size="20px">
					<p class="contact"><label for="name">To Address  </label></p>
					<input type="text" name="toAddress"size="20px">
					<p class="contact"><label for="name">CC Address  </label></p>
					<input type="text" name="ccAddress"size="20px">
					<p class="contact"><label for="name">BCC Address  </label></p>
					<input type="text" name="bccAddress"size="20px">
					<p class="contact"><label for="name">Override Address  </label></p>
					<select required="" name="overrideAddress" >
						<option value="no">no</option>
						<option value="yes">yes</option>
					</select>
					<p class="contact"><label for="name">Send Mail individually  </label></p>
					<select required="" name="sendMailIndividually" >
						<option value="no">no</option>
						<option value="yes">yes</option>
					</select>
					<!--<input type="checkbox" name="overrideAddress"size="20px">-->
					 <p class="contact"><label for="name">Notification Interval</label></p> 
					<input type="text" required="" name="notificationInterval"size="20px">
					 <p class="contact"><label for="name">Start Time</label></p> 
					<input type="text" name="startTime"size="20px">
					<input type="submit" value="submit">		
            	</form>

            </div>
            		<a href="bugList.jsp"> View Report List </a>

        </div>
	</body>	
</html>


