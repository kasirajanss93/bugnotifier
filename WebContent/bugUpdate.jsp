<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  
	pageEncoding="ISO-8859-1"%>
<%@ page import="oracle.apps.BugNotifier.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.*" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title> Bug Notifier   </title>
		 <link rel="stylesheet" type="text/css" href="style.css" media="all" />
    	<link rel="stylesheet" type="text/css" href="demo.css" media="all" />
	</head>	
<body>

	<div class="container">		
			<header>
				<a href="/bugNotifier"><h1>Bug Notifier</h1></a>
            </header>
            <div  class="form">
            	<form id="contactform" action="XMLUpdator"  method="POST">
            	<%
					XMLUpdator xmlUpdator=new XMLUpdator();
 					Map<String,String> bugInfoValues = new LinkedHashMap<String,String>();
    				bugInfoValues=xmlUpdator.getBugInformation(request.getParameter("id"));
    				Iterator it = bugInfoValues.entrySet().iterator();
        			while (it.hasNext()) {
            			Map.Entry pair = (Map.Entry)it.next();
            			if(pair.getKey().equals("selectClause")||pair.getKey().equals("fromClause")||pair.getKey().equals("whereClause")) {            		
				%>
					 <p class="contact"><label for="name"><%=pair.getKey()%> </label></p>
					 <textarea rows="4" cols="50" required="" name="<%=pair.getKey()%>"><%=pair.getValue()%></textarea>
					 
				<% 		}
						else if(!pair.getKey().equals("#text")) {
				%>
					 <p class="contact"><label for="name"><%=pair.getKey()%> </label></p>
					 <input type="text" name="<%=pair.getKey()%>"size="20px" value="<%=pair.getValue()%>">

				<%
						}
					}%>
				<input type="hidden" required="" name="Id" size="20px" value=<%=request.getParameter("id")%>>
				</br><input type="submit" value="submit">

            	</form>

            </div>
	
</body>
</html>