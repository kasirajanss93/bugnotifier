<%@ page language="java" contentType="text/html; charset=ISO-8859-1"  
	pageEncoding="ISO-8859-1"%>
<%@ page import="oracle.apps.BugNotifier.*" %>
<%@ page import="java.util.Map" %>
<%@ page import="java.util.LinkedHashMap" %>
<%@ page import="java.util.*" %>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title> Bug Notifier   </title>
		 <link rel="stylesheet" type="text/css" href="style.css" media="all" />
    	<link rel="stylesheet" type="text/css" href="demo.css" media="all" />
	</head>	
<body>

	<div class="container">		
			<header>
				<a href="/bugNotifier"><h1>Bug Notifier</h1></a>
            </header>
            <div  class="form">
            	<%
					XMLUpdator xmlUpdator=new XMLUpdator();
 					Map<String,String> bugInfoValues = new LinkedHashMap<String,String>();
    				bugInfoValues=xmlUpdator.getBugList();
    				Iterator it = bugInfoValues.entrySet().iterator();
        			while (it.hasNext()) {
            			Map.Entry pair = (Map.Entry)it.next();
            		
				%>
					<a href="/bugNotifier/bugUpdate.jsp?id=<%=pair.getKey() %>"> <%=pair.getValue()%> </a></br>
				<% }%>
	
            	</form>

            </div>
	
</body>
</html>